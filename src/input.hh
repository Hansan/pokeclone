#pragma once

#include "utils.hh"

#include <bitset>
#include <variant>

enum class KeyCommand {
  unbound,
  moveUp,
  moveDown,
  moveLeft,
  moveRight,
  interact,
  accept,
  cancel,
  openMenu,
  closeMenu,
  end,
  quit,
  toggleEditor,
  nextEditBGitem,
  previousEditBGitem,
  nextEditFGitem,
  previousEditFGitem,
  saveMap,
};

enum class Keycode {
  invalid = 0,
  enter = '\r',
  backspace = '\b',
  tab = '\t',
  escape = '\033',

  space = ' ',
  exclamation = '!',
  quote = '"',
  hash = '#',
  dollar = '$',
  percent = '%',
  ampersand = '&',
  apostrophe = '\'',
  openparen = '(',
  closeparen = ')',
  asterisk = '*',
  plus = '+',
  comma = ',',
  minus = '-',
  period = '.',
  slash = '/',

  key0 = '0',
  key1 = '1',
  key2 = '2',
  key3 = '3',
  key4 = '4',
  key5 = '5',
  key6 = '6',
  key7 = '7',
  key8 = '8',
  key9 = '9',

  colon = ':',
  semicolon = ';',
  less = '<',
  equal = '=',
  greater = '>',
  question = '?',
  at = '@',

  // screw uppercase

  openbracket = '[',
  backslash = '\\',
  closebracket = ']',
  caret = '^',
  underscore = '_',
  grave = '`',

  a = 'a',
  b = 'b',
  c = 'c',
  d = 'd',
  e = 'e',
  f = 'f',
  g = 'g',
  h = 'h',
  i = 'i',
  j = 'j',
  k = 'k',
  l = 'l',
  m = 'm',
  n = 'n',
  o = 'o',
  p = 'p',
  q = 'q',
  r = 'r',
  s = 's',
  t = 't',
  u = 'u',
  v = 'v',
  w = 'w',
  x = 'x',
  y = 'y',
  z = 'z',

  up,
  down,
  left,
  right,
};

struct Event {
  struct Invalid {};

  struct Mouse {
    struct Motion {
      bool rButtonHeld {};
      Point motion {};
    };

    struct Wheel {
      int y {}; // which direction it spun.
    };

    struct Button {
      Point cursorPosition {};
      bool lButtonDown {};
    };
  };

  struct Key {
    Keycode keycode {};
  };

  struct Quit {};

  std::variant<Invalid, Key, Mouse::Motion, Mouse::Wheel, Mouse::Button, Quit>
      type {};
};

struct KeyBind {
  KeyCommand command;
  Keycode key;

  [[nodiscard]] friend auto constexpr operator==(KeyBind const& lhs,
                                                 KeyBind const& rhs) -> bool {
    return lhs.command == rhs.command and lhs.key == rhs.key;
  }
};

using ModBitSet = std::bitset<12>;

// Roll own scuffed bit-or operation since std::bitset's isn't
// constexpr for whatever reason.
auto constexpr operator|(ModBitSet const& lhs, ModBitSet const& rhs)
    -> ModBitSet {
  auto tmp = std::uintmax_t {};
  for (std::size_t i {0}; i < lhs.size(); ++i) {
    tmp |= static_cast<unsigned>(lhs[i] or rhs[i]) << i;
  }
  return tmp;
}

struct Modkey {
  auto static constexpr lshift = ModBitSet {0b0000'0000'0001};
  auto static constexpr rshift = ModBitSet {0b0000'0000'0010};
  auto static constexpr lctrl = ModBitSet {0b0000'0000'0100};
  auto static constexpr rctrl = ModBitSet {0b0000'0000'1000};
  auto static constexpr lalt = ModBitSet {0b0000'0001'0000};
  auto static constexpr ralt = ModBitSet {0b0000'0010'0000};
  auto static constexpr lmeta = ModBitSet {0b0000'0100'0000};
  auto static constexpr rmeta = ModBitSet {0b0000'1000'0000};
  auto static constexpr numLock = ModBitSet {0b0001'0000'0000};
  auto static constexpr capsLock = ModBitSet {0b0010'0000'0000};
  auto static constexpr altgr = ModBitSet {0b0100'0000'0000};

  auto static constexpr shift = lshift | rshift;
  auto static constexpr ctrl = lctrl | rctrl;
  auto static constexpr alt = lalt | ralt;
  auto static constexpr meta = lmeta | rmeta;
};

auto handle_events() -> void;
