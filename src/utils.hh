#pragma once

#include <cassert>
#include <cstddef>
#include <cstdint>
#include <functional>

namespace Color {

struct RGBA {
  std::uint8_t static constexpr maxChannelValue {0xFF};

  struct Alpha {
    std::uint8_t static constexpr opaque {maxChannelValue};
    std::uint8_t static constexpr transparent {0};
  };

  std::uint8_t r {0};
  std::uint8_t g {0};
  std::uint8_t b {0};
  std::uint8_t a {Alpha::opaque};
};

RGBA static constexpr black {0, 0, 0};
RGBA static constexpr red {RGBA::maxChannelValue, 0, 0};
RGBA static constexpr green {0, RGBA::maxChannelValue, 0};
RGBA static constexpr blue {0, 0, RGBA::maxChannelValue};
RGBA static constexpr magenta {RGBA::maxChannelValue, 0, RGBA::maxChannelValue};
RGBA static constexpr white {RGBA::maxChannelValue, RGBA::maxChannelValue,
                             RGBA::maxChannelValue};

} // namespace Color

template <typename T>
struct PointGeneric {
  T x;
  T y;

  auto constexpr operator==(PointGeneric<T> const& other) const -> bool {
    return x == other.x and y == other.y;
  }

  auto constexpr operator!=(PointGeneric<T> const& other) const -> bool {
    return not(*this == other);
  }
};

namespace std {

template <typename T>
struct hash<PointGeneric<T>> {
  auto operator()(PointGeneric<T> const& p) const noexcept -> std::size_t {
    auto const hasher = std::hash<T> {};
    auto const h1 = hasher(p.x);
    auto const h2 = hasher(p.y);
    return h1 ^ (h2 << 1U);
  }
};

} // namespace std

using Point = PointGeneric<int>;

template <typename T>
struct RectGeneric {
  T x;
  T y;
  T w;
  T h;

  struct Size {
    using value_type = T;

    T w;
    T h;

    auto constexpr operator==(Size const& other) const -> bool {
      return w == other.w and h == other.h;
    }

    auto constexpr operator!=(Size const& other) const -> bool {
      return not(*this == other);
    }
  };

  auto constexpr operator==(RectGeneric<T> const& other) const -> bool {
    return x == other.x and y == other.y and w == other.w and h == other.h;
  }

  auto constexpr operator!=(RectGeneric<T> const& other) const -> bool {
    return not(*this == other);
  }

  [[nodiscard]] auto constexpr contains(
      PointGeneric<T> const& other) const noexcept -> bool {
    return (x <= other.x and (x + w) > other.x and y <= other.y and
            (y + h) > other.y);
  }

  [[nodiscard]] auto constexpr to_index(PointGeneric<T> const& p) const noexcept
      -> std::size_t {
    assert(contains(p));
    auto const relativeX = p.x - x;
    auto const relativeY = p.y - y;

    return static_cast<std::size_t>(relativeY * w + relativeX);
  }
};

using Rect = RectGeneric<int>;

template <typename T>
struct RectNormalizedGeneric {
  using ThisType = RectNormalizedGeneric<T>;

  struct Size {
    using value_type = T;

    T w;
    T h;

    auto constexpr operator==(Size const& other) const -> bool {
      return w == other.w and h == other.h;
    }

    auto constexpr operator!=(Size const& other) const -> bool {
      return not(*this == other);
    }
  };

  T x;
  T y;
  T w;
  T h;

  auto constexpr operator==(ThisType const& other) const -> bool {
    return x == other.x and y == other.y and w == other.w and h == other.h;
  }

  auto constexpr operator!=(ThisType const& other) const -> bool {
    return not(*this == other);
  }
};

using RectNormalized = RectNormalizedGeneric<double>;

auto constexpr to_screen_space(RectNormalized const rect,
                               Rect::Size const windowSize) -> Rect {
  return {static_cast<Rect::Size::value_type>(rect.x * windowSize.w),
          static_cast<Rect::Size::value_type>(rect.y * windowSize.h),
          static_cast<Rect::Size::value_type>(rect.w * windowSize.w),
          static_cast<Rect::Size::value_type>(rect.h * windowSize.h)};
}

template <typename T>
[[nodiscard]] auto constexpr wrap(T value, T const lowerBound,
                                  T const upperBound, int const increment = 0)
    -> T {
  auto const rangeElemCount = upperBound - lowerBound + 1;

  value += increment;

  if (value > upperBound) {
    auto const excess = value - upperBound - 1;
    auto const offset = excess % rangeElemCount;
    value = lowerBound + offset;
  } else if (value < lowerBound) {
    auto const excess = lowerBound - value - 1;
    auto const offset = excess % rangeElemCount;
    value = upperBound - offset;
  }

  return value;
}

auto translate_char(char ch) -> int;
