#include "gamestate.hh"

#include "core.hh"
#include "ui.hh"

GameState::GameState(Program& program) : m_program {program} {
  auto& resourceManager = program.get_resource_manager();
  player.entity.textureHandle =
      resourceManager.create_texture("AshSpriteSheet.bmp");
  backgroundTextureHandle = resourceManager.create_texture("background.bmp");
  foregroundTextureHandle = resourceManager.create_texture("foreground.bmp");
  characterTextureHandle = resourceManager.create_texture("CharacterSet.bmp");

  // load all the maps.
  maps[MapID::ashhouse] = Map {std::string_view {"ashhouseupper.txt"}};
  maps[MapID::pallet] = Map {std::string_view {"ashhouselower.txt"}};

  sprite[static_cast<int>(Controls::down)] = {0, 0, textureTileSize,
                                              textureTileSize};

  sprite[static_cast<int>(Controls::right)] = {
      textureTileSize, 0, textureTileSize, textureTileSize};

  sprite[static_cast<int>(Controls::up)] = {0, textureTileSize, textureTileSize,
                                            textureTileSize};

  sprite[static_cast<int>(Controls::left)] = {textureTileSize, textureTileSize,
                                              textureTileSize, textureTileSize};

  auto constexpr symbolSpriteSheetSpriteSize = 8;
  auto constexpr symbolSpriteSheetWidth = 10;
  auto constexpr symbolSpriteSheetHeight = 10;
  auto constexpr symbolSpriteSheetSpriteCount =
      symbolSpriteSheetWidth * symbolSpriteSheetHeight;

  // initializing symbol sprite sheet locations
  for (int i = 0; i < symbolSpriteSheetSpriteCount; i++) {
    auto const y = i / symbolSpriteSheetWidth;
    auto const x = i % symbolSpriteSheetWidth;
    symbolSet[i] = {
        x * symbolSpriteSheetSpriteSize,
        y * symbolSpriteSheetSpriteSize,
        symbolSpriteSheetSpriteSize,
        symbolSpriteSheetSpriteSize,
    };
  }

  set_menu(UIBox::MenuID::mainMenu);
}
