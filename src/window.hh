#pragma once

#include "utils.hh"

#include <filesystem>

class PlatformWindow;

class Window {
public:
  // These functions are platform dependent and need to be implemented by the
  // platform layer.
  explicit Window(PlatformWindow& platformWindow);
  Window(Window const&) = delete;
  Window(Window&&) noexcept = delete;
  auto operator=(Window const&) = delete;
  auto operator=(Window&&) noexcept = delete;
  ~Window() noexcept = default;

  auto change_scale(int scale);

  // These functions are platform independent and should be implemented in the
  // platform independent part of the code.
  [[nodiscard]] auto get_width() const noexcept -> int { return m_width; }
  [[nodiscard]] auto get_height() const noexcept -> int { return m_height; }

  [[nodiscard]] auto get_size() const noexcept -> Rect::Size {
    return {m_width, m_height};
  }

  [[nodiscard]] auto get_scale() const noexcept -> int { return m_scale; }

  [[nodiscard]] auto get_exe_path() const noexcept
      -> std::filesystem::path const& {
    return m_exePath;
  }

  [[nodiscard]] auto get_sprites_path() const noexcept
      -> std::filesystem::path const& {
    return m_spritesPath;
  }

  [[nodiscard]] auto get_maps_path() const noexcept
      -> std::filesystem::path const& {
    return m_mapsPath;
  }

  auto get_platform_window() noexcept -> PlatformWindow& {
    return m_platformWindow;
  }

  auto static constexpr baseWidth = 160;
  auto static constexpr baseHeight = 144;

private:
  PlatformWindow& m_platformWindow;

  std::filesystem::path const& m_exePath;
  std::filesystem::path const& m_spritesPath;
  std::filesystem::path const& m_mapsPath;

  int m_scale {};
  int m_width {};
  int m_height {};
};
