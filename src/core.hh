#pragma once

#include "gamestate.hh"
#include "program.hh"

auto get_program() -> Program&;
auto get_game_state() -> GameState&;
auto run(Program& program) -> void;
