#include "utils.hh"

#include <cctype>

auto translate_char(char ch) -> int {
  if (isupper(ch) != 0) {
    return ch - 'A';
  }

  if (islower(ch) != 0) {
    return ch - 'a' + 26;
  }

  char specials[20] = {'*', '(', ')', ':', ';', '[', ']', '\\', '-', '?',
                       '!', '$', '&', '/', '.', ',', '>', ' ',  ' ', ' '};

  for (int i = 0; i < 20; i++) {
    if (specials[i] == ch) {
      return 52 + i;
    }
  }
  return '\\';
}
