#pragma once

#include "pokemon.hh"

auto poke_battle() -> void;
auto run_from_battle() -> void;
auto set_moves() -> void;
auto use_move() -> void;

extern Pokemon* currentPokemon;
extern Pokemon* currentOpponent;
