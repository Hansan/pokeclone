#include "ui.hh"

#include "battle.hh"
#include "core.hh"
#include "data.hh"
#include "gamestate.hh"
#include "level.hh"
#include "move.hh"
#include "pokemon.hh"

#include <cassert>
#include <cstdio>

auto static pokedex_menu() -> void;
auto static pokedex_select_menu() -> void;
auto static poke_menu() -> void;
auto static pokemenu_select_menu() -> void;
auto static item_menu() -> void;
auto static trainer_menu() -> void;
auto static save_game() -> void;
auto static options_menu() -> void;

auto static battle_moves_menu() -> void;

auto static new_game() -> void;

auto static nothing() -> void;

auto static arrow_init() -> void;

auto static get_menu_box(UIBox::MenuID menuID) -> UIBox const*;
auto static previous_ui_group() -> void;
auto static push_menu(UIBox::MenuID menuID) -> void;
auto static press_menu_button() -> void;

UIGroupStack uiGroupStack;

auto static constexpr normalizedSymbolWidth = GameState::normalizedSymbolSize.w;
auto static constexpr normalizedSymbolHeight =
    GameState::normalizedSymbolSize.h;

auto static constexpr normalizedTileWidth = GameState::normalizedTileSize.w;
auto static constexpr normalizedTileHeight = GameState::normalizedTileSize.h;

// MAIN MENU
UIBox const mainMenuBox = {
    1,
    UIBox::MenuID::mainMenu,
    {
        {"NEW GAME", new_game},
        {"OPTIONS", options_menu},
    },
    0,
    {0, 0, 7. / 9., normalizedSymbolHeight * 6},
    {2 * normalizedSymbolWidth, normalizedSymbolHeight,
     mainMenuBox.bgRectNormalized.w - 3 * normalizedSymbolWidth,
     4 * normalizedSymbolHeight},
    normalizedTileWidth,
    normalizedTileHeight};

// PAUSE MENU
UIBox const pauseMenuBox = {
    1,
    UIBox::MenuID::pause,
    {{"POKeDEX", pokedex_menu},
     {"POKeMON", poke_menu},
     {"ITEM", item_menu},
     {"NAME", trainer_menu},
     {"SAVE", save_game},
     {"OPTION", options_menu},
     {"EXIT", exit_menu}},
    0,
    {1. / 2., 0, 1. / 2., 8. / 9.},
    {pauseMenuBox.bgRectNormalized.x + 2 * normalizedSymbolWidth,
     pauseMenuBox.bgRectNormalized.y + 2 * normalizedSymbolHeight,
     pauseMenuBox.bgRectNormalized.w - 3 * normalizedSymbolWidth,
     7. / 9. - 2 * normalizedSymbolHeight},
    normalizedTileWidth,
    normalizedTileHeight};

// POKEDEX MENU -- TODO

UIBox const pokedexMenuBox = {0,
                              UIBox::MenuID::pokedex,
                              {{"Charmander", pokedex_select_menu}},
                              0,
                              {0, 0, 1, 1},
                              {0, 0, 1, 1},
                              0,
                              0};

// POKEMON MENU -- TODO
UIBox const pokemonMenuBox = {1,
                              UIBox::MenuID::pokemon,
                              {{"", pokemenu_select_menu}},
                              0,
                              {0, 0, 1, 7. / 9.},
                              {0, 0, 1, 7. / 9.},
                              normalizedTileWidth,
                              normalizedTileHeight};

// ITEM MENU -- TODO
// TRAINER MENU -- TODO
// SAVE MENU -- TODO
UIBox const saveMenuBox = {
    1,
    UIBox::MenuID::save,
    {{"YES", nothing}, {"NO", exit_menu}},
    0,
    {0, 7 * normalizedSymbolHeight, 6 * normalizedSymbolWidth,
     5 * normalizedSymbolHeight},
    {2 * normalizedSymbolWidth, 8 * normalizedSymbolHeight,
     3 * normalizedSymbolWidth, 3 * normalizedSymbolHeight},
    normalizedTileWidth,
    normalizedTileHeight};

// OPTIONS MENU -- TODO
UIBox const optionsMenuBox = {
    1,
    UIBox::MenuID::options,
    {
        {"TEXT SPEED :", nothing},
        {"ANIMATION  :", nothing},
        {"BATTLESTYLE:", nothing},
        {"CANCEL", exit_menu},
    },
    0,
    {0, 0, 1, 1},
    {2 * normalizedSymbolWidth, normalizedSymbolHeight, 1, 1},
    normalizedTileWidth,
    normalizedTileHeight};

// BATTLE MENU
UIBox const battleMenuBox = {
    2,
    UIBox::MenuID::battle,
    {{"FIGHT", battle_moves_menu},
     {"\\M", nothing},
     {"ITEM", nothing},
     {"RUN", run_from_battle}},
    0,
    {8 * normalizedSymbolWidth, 2. / 3., 1. - 8 * normalizedSymbolWidth,
     1. / 3.},
    {battleMenuBox.bgRectNormalized.x + 2 * normalizedSymbolWidth,
     battleMenuBox.bgRectNormalized.y + 2 * normalizedSymbolHeight,
     battleMenuBox.bgRectNormalized.w - 3 * normalizedSymbolWidth,
     battleMenuBox.bgRectNormalized.h - 3 * normalizedSymbolHeight},
    6 * normalizedSymbolWidth,
    2 * normalizedSymbolHeight};

// BATTLE MOVES MENU
UIBox const battleMovesBox = {
    1,
    UIBox::MenuID::battlemoves,
    {
        {"", use_move},
        {"", use_move},
        {"", use_move},
        {"", use_move},
    },
    0,
    {4 * normalizedSymbolWidth, 2. / 3., 1. - 5 * normalizedSymbolWidth,
     1. / 3},
    {
        battleMovesBox.bgRectNormalized.x + 2 * normalizedSymbolWidth,
        battleMovesBox.bgRectNormalized.y + normalizedSymbolHeight,
        battleMovesBox.bgRectNormalized.w - 3 * normalizedSymbolWidth,
        battleMovesBox.bgRectNormalized.h - 3 * normalizedSymbolHeight,
    },
    0,
    normalizedSymbolHeight};

auto UIBox::message_box(UIText const msg) -> UIBox {
  UIBox box = {};
  auto constexpr twoThirds = 2. / 3.;
  auto constexpr oneThird = 1. / 3.;
  box.bgRectNormalized = {0, twoThirds, 1, oneThird};
  box.fgRectNormalized = {normalizedSymbolWidth,
                          box.bgRectNormalized.y + normalizedSymbolHeight,
                          box.bgRectNormalized.w - 3 * normalizedSymbolWidth,
                          4 * normalizedSymbolHeight};

  box.regions.push_back(msg);

  return box;
}

auto set_menu(UIBox::MenuID menuID) -> void {
  uiGroupStack.clear();
  UIGroup uigroup = {};

  // FIXME: rename get_menu_box
  auto const* menuBox = get_menu_box(menuID);
  if (menuBox != nullptr) {
    uigroup.add(*menuBox);
    uiGroupStack.push_back(uigroup);

    // battlemoves needs to do extra setup stuff
    if (menuID == UIBox::MenuID::battlemoves) {
      set_moves();
    }
  }
}

auto exit_menu() -> void { previous_ui_group(); }

auto open_pause_menu() -> void {
  assert(uiGroupStack.empty());
  push_menu(UIBox::MenuID::pause);
}

auto ui_interact() -> void {
  assert(not uiGroupStack.empty());
  auto const& uiGroup = uiGroupStack.back();
  if (uiGroup.has_menu_box()) {
    press_menu_button();
  } else {
    // There is no menu in the ui group
    // TODO: Handle advancing text and potentially
    //       executing code when done.
    //       ATM just go to previous ui group.
    previous_ui_group();
  }
}

auto ui_cancel() -> void {
  assert(not uiGroupStack.empty());
  auto const& uiGroup = uiGroupStack.back();
  if (uiGroup.has_menu_box()) {
    // there are cases when this shouldn't happen
    // (e.g. in the root battle menu)
    previous_ui_group();
  } else {
    // There is no menu in the ui group
    // TODO: Handle advancing text and potentially
    //       executing code when done.
    //       ATM just go to previous ui group.
    previous_ui_group();
  }
}

auto static new_game() -> void {
  load_map(MapID::ashhouse);
  auto& gameState = get_game_state();
  gameState.state = GameState::State::overworld;

  auto const tileSize = gameState.get_tile_size();

  // Player init
  gameState.player.entity.pos.x = 4;
  gameState.player.entity.pos.y = 4;
  gameState.player.entity.hitbox.x = gameState.player.entity.pos.x * tileSize;
  gameState.player.entity.hitbox.y = gameState.player.entity.pos.y * tileSize;
  gameState.player.entity.hitbox.w = tileSize;
  gameState.player.entity.hitbox.h = tileSize;

  gameState.player.entity.textureRect =
      gameState.sprite[static_cast<int>(Controls::down)];

  gameState.camera.pos.x = gameState.player.entity.hitbox.x - 4 * tileSize;
  gameState.camera.pos.y = gameState.player.entity.hitbox.y - 4 * tileSize;
}

auto static previous_ui_group() -> void {
  assert(not uiGroupStack.empty());
  auto* menuBox = uiGroupStack.back().get_menu_box();
  if (menuBox != nullptr) {
    if (menuBox->menuid == UIBox::MenuID::mainMenu) {
      return;
    }
  }

  uiGroupStack.pop_back();
  if (uiGroupStack.empty()) {
    auto& gameState = get_game_state();
    gameState.state = GameState::State::overworld;
  }
}

auto static get_menu_box(UIBox::MenuID menuID) -> UIBox const* {
  UIBox const* menuBox = nullptr;
  switch (menuID) {
  case UIBox::MenuID::mainMenu: {
    menuBox = &mainMenuBox;
  } break;
  case UIBox::MenuID::pause: {
    menuBox = &pauseMenuBox;
  } break;
  case UIBox::MenuID::battle: {
    menuBox = &battleMenuBox;
  } break;
  case UIBox::MenuID::none: {
  } break;
  case UIBox::MenuID::pokedex: {
    menuBox = &pokedexMenuBox;
  } break;
  case UIBox::MenuID::pokemon: {
    menuBox = &pokemonMenuBox;
  } break;
  case UIBox::MenuID::save: {
    menuBox = &saveMenuBox;
  } break;
  case UIBox::MenuID::options: {
    menuBox = &optionsMenuBox;
  } break;
  case UIBox::MenuID::battlemoves: {
    menuBox = &battleMovesBox;
  } break;
  case UIBox::MenuID::items: // fallthrough
  case UIBox::MenuID::trainer: {
    printf("menu not implemented yet");
  } break;
  default: {
    printf("Error __func__ ");
  } break;
  }

  return menuBox;
}

auto static push_menu(UIBox::MenuID menuID) -> void {
  UIGroup uigroup = {};

  auto const* menuBox = get_menu_box(menuID);
  if (menuBox != nullptr) {
    uigroup.add(*menuBox);
    uiGroupStack.push_back(uigroup);

    // battlemoves needs to do extra setup stuff
    if (menuID == UIBox::MenuID::battlemoves) {
      set_moves();
    }
  }
}

auto static pokedex_menu() -> void {}

auto static poke_menu() -> void {
  create_party(); // temporary

  auto& party = get_game_state().player.party;
  puts(party[0].name);
  printf("id: %d\n", party[0].id);
  printf("lvl: %d\n", party[0].lvl);
  printf("atk: %d\n", party[0].stats.atk);
  printf("def: %d\n", party[0].stats.def);
  printf("spd: %d\n", party[0].stats.spd);
  printf("spc: %d\n", party[0].stats.spc);
  printf("hp: %d\n", party[0].hp);
  printf("status: %d\n", party[0].status);
  printf("type1: %d\n", party[0].type1);
  printf("type2: %d\n", party[0].type2);

  printf("---moves---\n");

  printf("%s:\n", party[0].moves[0].name);
  printf("pp: %d\n", party[0].moves[0].pp);
  printf("type: %d\n", party[0].moves[0].type);
  printf("power: %d\n", party[0].moves[0].power);

  printf("%s:\n", party[0].moves[1].name);
  printf("pp: %d\n", party[0].moves[1].pp);
  printf("type: %d\n", party[0].moves[1].type);
  printf("power: %d\n", party[0].moves[1].power);

  printf("%s:\n", party[0].moves[2].name);
  printf("pp: %d\n", party[0].moves[2].pp);
  printf("type: %d\n", party[0].moves[2].type);
  printf("power: %d\n", party[0].moves[2].power);

  printf("%s:\n", party[0].moves[3].name);
  printf("pp: %d\n", party[0].moves[3].pp);
  printf("type: %d\n", party[0].moves[3].type);
  printf("power: %d\n", party[0].moves[3].power);
  fflush(stdout);

  push_menu(UIBox::MenuID::pokemon);
}

auto static item_menu() -> void {}

auto static trainer_menu() -> void {}

auto static save_game() -> void {
  // push_menu(UIBox::MenuID::Save);
  UIGroup uigroup = {};
  static auto saveMessageBox =
      UIBox::message_box({"Do you want to save the game?"});

  uigroup.add(saveMessageBox);
  auto const* menuBox = get_menu_box(UIBox::MenuID::save);
  if (menuBox != nullptr) {
    uigroup.add(*menuBox);
    uiGroupStack.push_back(uigroup);
  }
}

auto static options_menu() -> void { push_menu(UIBox::MenuID::options); }

auto static press_menu_button() -> void {
  // TODO: this pattern of getting the menu box is very common.
  //       create function maybe?
  assert(not uiGroupStack.empty());
  auto* menuBox = uiGroupStack.back().get_menu_box();
  assert(menuBox);
  auto pos = static_cast<std::size_t>(menuBox->selection);
  menuBox->regions[pos].execute();
}

auto static nothing() -> void { fprintf(stderr, "doing nothing"); }

auto static pokemenu_select_menu() -> void {}

auto static pokedex_select_menu() -> void {}

auto static battle_moves_menu() -> void {
  push_menu(UIBox::MenuID::battlemoves);
}

struct MenuGridSize {
  const std::size_t width {};
  const std::size_t height {};

  constexpr MenuGridSize(std::size_t width_, std::size_t height_)
      : width {width_}, height {height_} {
    if (width == 0) {
      throw std::invalid_argument("The width of a MenuGrid cannot be 0");
    }
    if (height == 0) {
      throw std::invalid_argument("The height of a MenuGrid cannot be 0");
    }
  }
};

[[nodiscard]] constexpr auto
get_change_in_selection_index(std::size_t selection, MenuGridSize menuGridSize,
                              Direction direction) noexcept -> int {
  assert(selection < menuGridSize.width * menuGridSize.height);
  assert(menuGridSize.width <= std::numeric_limits<int>::max());
  assert(menuGridSize.height <= std::numeric_limits<int>::max());

  switch (direction) {
  case Direction::up: {
    const auto isFirstRow = selection < menuGridSize.width;
    return isFirstRow ? 0 : -static_cast<int>(menuGridSize.width);
  }
  case Direction::down: {
    const auto lastIndex = menuGridSize.height * menuGridSize.width - 1;
    const auto isLastRow = lastIndex - selection < menuGridSize.width;
    return isLastRow ? 0 : static_cast<int>(menuGridSize.width);
  }
  case Direction::right: {
    const auto currentColumn = selection % menuGridSize.width;
    const auto isLastColumn = currentColumn == (menuGridSize.width - 1);
    return isLastColumn ? 0 : 1;
  }
  case Direction::left: {
    const auto currentColumn = selection % menuGridSize.width;
    const auto isFirstColumn = currentColumn == 0;
    return isFirstColumn ? 0 : -1;
  }
  }
  // Invalid enum value.
  std::terminate();
};

static_assert(get_change_in_selection_index(0, {2, 1}, Direction::left) == 0);
static_assert(get_change_in_selection_index(0, {2, 1}, Direction::right) == 1);
static_assert(get_change_in_selection_index(0, {2, 1}, Direction::down) == 0);
static_assert(get_change_in_selection_index(0, {2, 1}, Direction::up) == 0);

static_assert(get_change_in_selection_index(1, {2, 1}, Direction::left) == -1);
static_assert(get_change_in_selection_index(1, {2, 1}, Direction::right) == 0);
static_assert(get_change_in_selection_index(1, {2, 1}, Direction::down) == 0);
static_assert(get_change_in_selection_index(1, {2, 1}, Direction::up) == 0);

static_assert(get_change_in_selection_index(0, {2, 2}, Direction::left) == 0);
static_assert(get_change_in_selection_index(0, {2, 2}, Direction::right) == 1);
static_assert(get_change_in_selection_index(0, {2, 2}, Direction::down) == 2);
static_assert(get_change_in_selection_index(0, {2, 2}, Direction::up) == 0);

static_assert(get_change_in_selection_index(2, {2, 2}, Direction::left) == 0);
static_assert(get_change_in_selection_index(2, {2, 2}, Direction::right) == 1);
static_assert(get_change_in_selection_index(2, {2, 2}, Direction::down) == 0);
static_assert(get_change_in_selection_index(2, {2, 2}, Direction::up) == -2);

[[nodiscard]] auto
UIBox::calculate_selection_after_move(Direction direction) const noexcept
    -> int {
  assert(selection >= 0);
  assert(not regions.empty());

  const auto rowCount = (regions.size() - 1) / columns + 1;
  const auto change = get_change_in_selection_index(
      selection, {static_cast<std::size_t>(columns), rowCount}, direction);
  const auto maxPos = static_cast<int>(regions.size() - 1);
  return std::clamp(selection + change, 0, maxPos);
}

auto UIBox::move_selection(Direction direction) -> void {
  selection = calculate_selection_after_move(direction);
}