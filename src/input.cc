#include "input.hh"

#include "core.hh"
#include "data.hh"
#include "gamestate.hh"
#include "inputmap.hh"
#include "level.hh"
#include "move.hh"
#include "ui.hh"
#include "utils.hh"

#include <algorithm>
#include <bitset>
#include <cassert>
#include <cctype>
#include <filesystem>
#include <iostream>
#include <optional>
#include <sstream>
#include <variant>

// Function declarations
auto static interact() -> void;

extern bool quitFlag;

static constexpr auto key_is_command(Keycode key, KeyCommand wantedCommand) -> bool {
  KeyBind const targetBind {wantedCommand, key};

  return std::ranges::any_of(inputMap, [targetBind](auto const& keyBind) {
    return keyBind == targetBind;
  });
}

static_assert(key_is_command(Keycode::left, KeyCommand::moveLeft));
static_assert(key_is_command(Keycode::z, KeyCommand::accept));
static_assert(key_is_command(Keycode::z, KeyCommand::interact));
static_assert(not key_is_command(Keycode::z, KeyCommand::moveDown));

auto static handle_edit_mouse_motion(Event::Mouse::Motion const& event)
    -> void {
  // if the right button is held down while moving mouse
  if (event.rButtonHeld) {
    auto& gameState = get_game_state();
    gameState.editingCamera.pos.x -= event.motion.x;
    gameState.editingCamera.pos.y -= event.motion.y;
  }
}

auto static handle_edit_mouse_wheel(Event::Mouse::Wheel const& event) -> void {
  auto& gameState = get_game_state();
  if (event.y > 0) {
    ++gameState.editingZoom;
  } else if (event.y < 0) {
    // limit how far you can zoom out
    if (gameState.editingZoom - 1 > -gameState.get_tile_size()) {
      --gameState.editingZoom;
    }
  }
}

auto static handle_edit_mouse_button_down(Event::Mouse::Button const& event)
    -> void {
  auto& gameState = get_game_state();

  auto const tileUnderCursorCoords = [&]() -> Point {
    auto currentTileSize = gameState.get_tile_size() + gameState.editingZoom;
    auto absoluteX = event.cursorPosition.x + gameState.editingCamera.pos.x;
    auto absoluteY = event.cursorPosition.y + gameState.editingCamera.pos.y;
    auto tileX = absoluteX / currentTileSize;
    auto tileY = absoluteY / currentTileSize;
    if (absoluteX < 0) {
      --tileX;
    }
    if (absoluteY < 0) {
      --tileY;
    }
    return {tileX, tileY};
  };

  if (event.lButtonDown) {
    auto const tileCoords = tileUnderCursorCoords();
    gameState.editingSelectionX = tileCoords.x;
    gameState.editingSelectionY = tileCoords.y;

    auto const modState = get_mod_state();
    if ((modState & Modkey::shift).any()) {
      auto& tile = gameState.editMap[tileCoords];
      tile.bgSpriteType = gameState.bgEditItemSprite;
    }
  }
}

auto static to_char(Keycode const key) -> std::optional<char> {
  // The Keycodes correspond to the ascii characters they represent.
  auto const c = static_cast<char>(key);
  if (std::isgraph(static_cast<unsigned char>(c)) != 0) {
    return c;
  }
  return std::nullopt;
}

auto static try_save_map(std::string_view const fileName) -> bool {
  auto const& gameState = get_game_state();
  auto const map = Map {gameState.editMap};
  return map.save(fileName);
}

auto static handle_edit_key_down(Event::Key const& event) -> void {
  auto& gameState = get_game_state();
  auto const key = event.keycode;
  // FIXME: these counts should be set during init phase.
  auto constexpr bgTextureCount = 5;
  auto constexpr fgTextureCount = 18;

  auto static isSaving = false;
  if (isSaving) {
    // FIXME: The keys q and e have already been checked in handle_events,
    // so it's impossible to use them in the filename.
    auto static fileName = std::string {};
    switch (key) {
    case Keycode::enter:
      if (not try_save_map(fileName)) {
        return;
      }
      fileName.clear();
      isSaving = false;
      break;
    case Keycode::backspace:
      if (not fileName.empty()) {
        fileName.pop_back();
      }
      break;
    default:
      auto const maybeC = to_char(key);
      if (maybeC) {
        fileName.push_back(*maybeC);
      }
    }
    std::cerr << "Filename: [" << fileName << "]\n";
  } else if (key_is_command(key, KeyCommand::nextEditBGitem)) {
    gameState.bgEditItemSprite = wrap(gameState.bgEditItemSprite, 1);
  } else if (key_is_command(key, KeyCommand::previousEditBGitem)) {
    gameState.bgEditItemSprite = wrap(gameState.bgEditItemSprite, -1);
  } else if (key_is_command(key, KeyCommand::nextEditFGitem)) {
    gameState.fgEditItemSprite = wrap(gameState.fgEditItemSprite, 1);
  } else if (key_is_command(key, KeyCommand::previousEditFGitem)) {
    gameState.fgEditItemSprite = wrap(gameState.fgEditItemSprite, -1);
  } else if (key_is_command(key, KeyCommand::saveMap)) {
    isSaving = true;
  }
}

auto static handle_edit_events(Event const& event) -> void {
  using std::holds_alternative, std::get;
  auto const& type = event.type;

  if (holds_alternative<Event::Key>(type)) {
    handle_edit_key_down(get<Event::Key>(type));
  } else if (holds_alternative<Event::Mouse::Motion>(type)) {
    handle_edit_mouse_motion(get<Event::Mouse::Motion>(type));
  } else if (holds_alternative<Event::Mouse::Wheel>(type)) {
    handle_edit_mouse_wheel(get<Event::Mouse::Wheel>(type));
  } else if (holds_alternative<Event::Mouse::Button>(type)) {
    handle_edit_mouse_button_down(get<Event::Mouse::Button>(type));
  }
}

auto static handle_menu_events(Event const& event) -> void {
  if (std::holds_alternative<Event::Key>(event.type)) {
    auto const key = std::get<Event::Key>(event.type).keycode;
    auto* menuBox = uiGroupStack.back().get_menu_box();
    if (menuBox != nullptr) {
      // ui contains menu
      if (key_is_command(key, KeyCommand::moveUp)) {
        menuBox->move_selection(Direction::up);
      } else if (key_is_command(key, KeyCommand::moveDown)) {
        menuBox->move_selection(Direction::down);
      } else if (key_is_command(key, KeyCommand::moveRight)) {
        menuBox->move_selection(Direction::right);
      } else if (key_is_command(key, KeyCommand::moveLeft)) {
        menuBox->move_selection(Direction::left);
      } else if (key_is_command(key, KeyCommand::closeMenu)) {
        if (menuBox->menuid == UIBox::MenuID::pause) {
          exit_menu();
        }
      } else if (key_is_command(key, KeyCommand::accept)) {
        ui_interact();
      } else if (key_is_command(key, KeyCommand::cancel)) {
        ui_cancel();
      }
    } else {
      // ui doesn't contain menu
      if (key_is_command(key, KeyCommand::accept)) {
        ui_interact();
      } else if (key_is_command(key, KeyCommand::cancel)) {
        ui_cancel();
      }
    }
  }
}

auto static handle_overworld_events(Event const& event) -> void {
  if (std::holds_alternative<Event::Key>(event.type)) {
    auto const key = std::get<Event::Key>(event.type).keycode;
    if (key_is_command(key, KeyCommand::moveUp)) {
      move_player(Direction::up);
    } else if (key_is_command(key, KeyCommand::moveDown)) {
      move_player(Direction::down);
    } else if (key_is_command(key, KeyCommand::moveRight)) {
      move_player(Direction::right);
    } else if (key_is_command(key, KeyCommand::moveLeft)) {
      move_player(Direction::left);
    } else if (key_is_command(key, KeyCommand::openMenu)) {
      open_pause_menu();
    }
  }
}

auto handle_events() -> void {
  auto& gameState = get_game_state();

  while (auto const maybeEvent = poll_event()) {
    auto const& event = *maybeEvent;
    // events that do the same thing in every state
    if (std::holds_alternative<Event::Quit>(event.type) or
        (std::holds_alternative<Event::Key>(event.type) and
         key_is_command(std::get<Event::Key>(event.type).keycode,
                        KeyCommand::quit))) {
      quitFlag = 1;
      return;
    }

    if (std::holds_alternative<Event::Key>(event.type) and
        key_is_command(std::get<Event::Key>(event.type).keycode,
                       KeyCommand::toggleEditor)) {
      if (gameState.state == GameState::State::editing) {
        gameState.state = gameState.stateBeforeEdit;
      } else {
        if (gameState.currentMapID != MapID::none) {
          gameState.stateBeforeEdit = gameState.state;
          gameState.state = GameState::State::editing;

          gameState.editMap.clear();
        } else {
          // TODO: allow selecting map
          std::cerr << "No map selected\n";
        }
      }
      // events that do specific things depending on the state
    } else if (gameState.state == GameState::State::editing) {
      handle_edit_events(event);
    } else if (not uiGroupStack.empty()) {
      handle_menu_events(event);
    } else if (gameState.state == GameState::State::overworld) {
      handle_overworld_events(event);
    }
  }
}

auto static interact() -> void {}
