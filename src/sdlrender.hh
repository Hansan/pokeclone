#pragma once

#include "utils.hh"

#include <SDL.h>

#include <string_view>
#include <vector>

class Surface {
public:
  Surface() = default;
  explicit Surface(std::string_view bmpFileName);
  Surface(Surface const&) = delete;
  auto operator=(Surface const&) = delete;
  Surface(Surface&& other) noexcept;
  auto operator=(Surface&& other) noexcept -> Surface&;
  ~Surface() noexcept { SDL_FreeSurface(m_surface); }

  [[nodiscard]] auto get() const -> SDL_Surface* { return m_surface; }

private:
  SDL_Surface* m_surface {nullptr};
};

class Texture {
public:
  Texture() = default;
  explicit Texture(std::string_view bmpFileName);
  explicit Texture(Surface const& surface);
  Texture(Texture const&) = delete;
  auto operator=(Texture const&) = delete;
  Texture(Texture&& other) noexcept;
  auto operator=(Texture&& other) noexcept -> Texture&;
  ~Texture() noexcept;

  [[nodiscard]] auto get() const -> SDL_Texture* { return m_texture; }

private:
  SDL_Texture* m_texture {nullptr};
};

class PlatformResourceManager {
public:
  [[nodiscard]] auto create_surface(std::string_view bmpFileName)
      -> std::size_t;
  [[nodiscard]] auto create_texture(Surface const& surface) -> std::size_t;
  [[nodiscard]] auto create_texture(std::string_view bmpFileName)
      -> std::size_t;

  auto get_texture(std::size_t const handle) -> Texture& {
    return m_textures.at(handle);
  }

  auto get_surface(std::size_t const handle) -> Surface& {
    return m_surfaces.at(handle);
  }

private:
  std::vector<Surface> m_surfaces;
  std::vector<Texture> m_textures;
};

auto render_copy(std::size_t textureHandle, Rect srcRect, Rect dstRect) -> void;
auto render_copy_normalized(std::size_t textureHandle, Rect srcRect,
                            RectNormalized dstRect) -> void;
auto render_clear() -> void;
auto render_present() -> void;
auto render_draw_rect_normalized(RectNormalized rect) -> void;
auto render_draw_rect(Rect rect) -> void;
auto render_fill_rect_normalized(RectNormalized rect) -> void;
auto render_fill_rect(Rect rect) -> void;
auto set_render_draw_color(Color::RGBA c) -> void;
