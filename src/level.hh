#pragma once

#include "data.hh"
#include "utils.hh"

#include <cstddef>
#include <filesystem>
#include <optional>
#include <string_view>
#include <vector>

enum class MapID {
  none,
  pallet,
  ashhouse,
};

enum class EncounterType {
  none,
  grass,
  cave,
  trainer,
};

struct Tile {
  // The enumerators must create a continuous range from first..last.
  enum class BGSpriteType {
    none,
    wall,
    floor,
    stairsDown,
    stairsUp,
    doormat,
    // WARNING: If you add a sprite type you also need to assign it to the
    // 'last' enumerator.

    first = none,
    last = doormat,
  };

  // The enumerators must create a continuous range from first..last.
  enum class FGSpriteType {
    none,
    blinds,
    tv,
    snes,
    potPlantBot,
    potPlantTop,
    bedTop,
    bedBottom,
    tableNW,
    tableNE,
    tableSW,
    tableSE,
    computerTop,
    computerBottom,
    bookshelfTop,
    bookshelfBottom,
    stool,
    flowerTableNW,
    flowerTableNE,
    // WARNING: If you add a sprite type you also need to assign it to the
    // 'last' enumerator.

    first = none,
    last = flowerTableNE,
  };

  bool solid {false};
  EncounterType encounterType {EncounterType::none};
  FGSpriteType fgSpriteType {FGSpriteType::none};
  BGSpriteType bgSpriteType {BGSpriteType::none};
};

class Map {
public:
  Map() = default;
  explicit Map(std::string_view fileName);
  explicit Map(std::filesystem::path const& filePath);
  explicit Map(std::unordered_map<Point, Tile> editMap);

  [[nodiscard]] auto save(std::string_view fileName) const -> bool;

  [[nodiscard]] auto get_rect() const noexcept -> Rect {
    return {0, 0, m_width, m_height};
  }

  [[nodiscard]] auto get_width() const noexcept -> int { return m_width; }

  [[nodiscard]] auto get_height() const noexcept -> int { return m_height; }

  [[nodiscard]] auto tile_at(std::size_t const index) -> Tile& {
    return m_tiles.at(index);
  }

  [[nodiscard]] auto tile_at(std::size_t const index) const -> Tile const& {
    return m_tiles.at(index);
  }

  [[nodiscard]] auto tile_at(Point const point) -> Tile& {
    auto const i = get_rect().to_index(point);
    return tile_at(i);
  }

  [[nodiscard]] auto tile_at(Point const point) const -> Tile const& {
    auto const i = get_rect().to_index(point);
    return tile_at(i);
  }

private:
  [[nodiscard]] auto save(std::filesystem::path const& filePath) const -> bool;

  int m_width {0};
  int m_height {0};

  std::vector<Tile> m_tiles {};
};

[[nodiscard]] auto constexpr to_texture_rect(Tile::FGSpriteType const st)
    -> std::optional<Rect> {
  using ST = Tile::FGSpriteType;

  switch (st) {
  case ST::none:
    return std::nullopt;
  case ST::blinds:
  case ST::tv:
  case ST::snes:
  case ST::potPlantBot:
  case ST::potPlantTop:
  case ST::bedTop:
  case ST::bedBottom:
  case ST::tableNW:
  case ST::tableNE:
  case ST::tableSW:
  case ST::tableSE:
  case ST::computerTop:
  case ST::computerBottom:
  case ST::bookshelfTop:
  case ST::bookshelfBottom:
  case ST::stool:
  case ST::flowerTableNW:
  case ST::flowerTableNE:
    // Assumes Tile::FGSpriteType::none has value 0 and every other enumerator
    // continues from 1..lastenumvalue.
    auto const index = static_cast<std::size_t>(st) - 1;
    return textureRects.at(index);
  }
  // Either function was called with an invalid enum value or not all enum
  // values have been handled in the switch.
  std::terminate();
}

[[nodiscard]] auto constexpr to_texture_rect(Tile::BGSpriteType const st)
    -> std::optional<Rect> {
  using ST = Tile::BGSpriteType;

  switch (st) {
  case ST::none:
    return std::nullopt;
  case ST::wall:
  case ST::floor:
  case ST::stairsDown:
  case ST::stairsUp:
  case ST::doormat:
    // Assumes Tile::FGSpriteType::none has value 0 and every other enumerator
    // continues from 1..lastenumvalue.
    auto const index = static_cast<std::size_t>(st) - 1;
    return textureRects.at(index);
  }
  // Either function was called with an invalid enum value or not all enum
  // values have been handled in the switch.
  std::terminate();
}

[[nodiscard]] auto constexpr wrap(Tile::FGSpriteType const st,
                                  int const increment) -> Tile::FGSpriteType {
  using ST = Tile::FGSpriteType;
  return static_cast<ST>(wrap(static_cast<int>(st), static_cast<int>(ST::first),
                              static_cast<int>(ST::last), increment));
}

[[nodiscard]] auto constexpr wrap(Tile::BGSpriteType const st,
                                  int const increment) -> Tile::BGSpriteType {
  using ST = Tile::BGSpriteType;
  return static_cast<ST>(wrap(static_cast<int>(st), static_cast<int>(ST::first),
                              static_cast<int>(ST::last), increment));
}

auto load_map(MapID newMap) -> void;
