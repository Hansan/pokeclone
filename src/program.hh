#pragma once

#include "resourcemanager.hh"
#include "window.hh"

#include <filesystem>
#include <random>

class Program {
public:
  explicit Program(Window& window, ResourceManager& resourceManager)
      : m_window {window},
        m_resourceManager {resourceManager}, m_exePath {window.get_exe_path()},
        m_spritesPath {window.get_sprites_path()},
        m_mapsPath {window.get_maps_path()} {}
  Program(Program const&) = delete;
  Program(Program&&) noexcept = delete;
  auto operator=(Program const&) = delete;
  auto operator=(Program&&) noexcept = delete;
  ~Program() noexcept = default;

  [[nodiscard]] auto get_resource_manager() const noexcept -> ResourceManager& {
    return m_resourceManager;
  }

  [[nodiscard]] auto get_window() const noexcept -> Window& { return m_window; }

  [[nodiscard]] auto get_exe_path() const noexcept
      -> std::filesystem::path const& {
    return m_exePath;
  }

  [[nodiscard]] auto get_sprites_path() const noexcept
      -> std::filesystem::path const& {
    return m_spritesPath;
  }

  [[nodiscard]] auto get_maps_path() const noexcept
      -> std::filesystem::path const& {
    return m_mapsPath;
  }

  [[nodiscard]] auto get_random_device() noexcept -> std::random_device& {
    return m_rd;
  }

private:
  Window& m_window;
  ResourceManager& m_resourceManager;

  std::random_device m_rd;

  std::filesystem::path const& m_exePath;
  std::filesystem::path const& m_spritesPath;
  std::filesystem::path const& m_mapsPath;
};
