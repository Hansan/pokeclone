#pragma once

#include "input.hh"

#include <array>

std::array static constexpr inputMap = {
    KeyBind {KeyCommand::moveUp, Keycode::up},
    KeyBind {KeyCommand::moveDown, Keycode::down},
    KeyBind {KeyCommand::moveLeft, Keycode::left},
    KeyBind {KeyCommand::moveRight, Keycode::right},
    KeyBind {KeyCommand::interact, Keycode::z},
    KeyBind {KeyCommand::accept, Keycode::z},
    KeyBind {KeyCommand::cancel, Keycode::x},
    KeyBind {KeyCommand::openMenu, Keycode::enter},
    KeyBind {KeyCommand::closeMenu, Keycode::enter},
    KeyBind {KeyCommand::quit, Keycode::q},
    KeyBind {KeyCommand::toggleEditor, Keycode::e},
    KeyBind {KeyCommand::previousEditBGitem, Keycode::key1},
    KeyBind {KeyCommand::nextEditBGitem, Keycode::key2},
    KeyBind {KeyCommand::previousEditFGitem, Keycode::key3},
    KeyBind {KeyCommand::nextEditFGitem, Keycode::key4},
    KeyBind {KeyCommand::saveMap, Keycode::s},
};
