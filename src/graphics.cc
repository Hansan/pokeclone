#include "graphics.hh"

#include "battle.hh"
#include "core.hh"
#include "data.hh"
#include "gamestate.hh"
#include "level.hh"
#include "pokemon.hh"
#include "ui.hh"
#include "utils.hh"

#include "sdlrender.hh"

#include <cassert>
#include <cctype>
#include <cstdio>
#include <iostream>
#include <string>
#include <utility>

using std::string;
using std::string_view;

// prototypes
auto static render_level() -> void;
auto static render_entities() -> void;
auto static render_ui_normalized() -> void;
auto static render_editing_ui() -> void;
auto static render_battle_normalized() -> void;
auto static render_editing_level() -> void;
auto static draw_message_box(UIBox const& box, string_view str) -> string_view;
auto static draw_text_line(string_view str, Rect lineRect) -> string_view;
auto static draw_text_line_normalized(string_view str, RectNormalized lineRect)
    -> string_view;
auto static draw_word(string_view word, Rect wordRect) -> void;
auto static draw_character(char ch, Rect chRect) -> void;
auto static render_ui_box_normalized(UIBox const& uibox) -> void;

auto static constexpr normalizedSymbolWidth = GameState::normalizedSymbolSize.w;
auto static constexpr normalizedSymbolHeight =
    GameState::normalizedSymbolSize.w;

auto static constexpr normalizedTileWidth = GameState::normalizedTileSize.w;
auto static constexpr normalizedTileHeight = GameState::normalizedTileSize.w;

// should probably be moved somewhere else
UIBox const messageBox = {
    0,
    {},
    {},
    0,
    {0, 2. / 3., 1, 1. / 3.},
    {messageBox.bgRectNormalized.x + normalizedSymbolWidth,
     messageBox.bgRectNormalized.y + normalizedSymbolHeight,
     messageBox.bgRectNormalized.w - 3 * normalizedSymbolWidth,
     4 * normalizedSymbolHeight},
    0,
    0};

auto render() -> void {
  set_render_draw_color(Color::white);
  render_clear();

  auto const& gameState = get_game_state();

  if (gameState.state == GameState::State::editing) {
    // TODO
    render_editing_level();
    render_editing_ui();
  } else {
    render_level();
    render_ui_normalized();
  }

  render_present();
}

auto static get_tile_rect(Point const p, int const size,
                          GameState::Camera const c) -> Rect {
  return {(p.x * size - c.pos.x), (p.y * size - c.pos.y), size, size};
}

auto static render_level() -> void {
  auto const& gameState = get_game_state();
  if (gameState.state != GameState::State::overworld) {
    return;
  }
  set_render_draw_color(Color::black);
  render_clear();

  auto const& map = gameState.maps.at(gameState.currentMapID);
  auto const mapRect = map.get_rect();
  for (int y = 0; y < map.get_height(); ++y) {
    for (int x = 0; x < map.get_width(); ++x) {
      auto const& tile = map.tile_at(Point {x, y});

      auto const tileRect =
          get_tile_rect({x, y}, gameState.get_tile_size(), gameState.camera);

      if (auto const maybeTextureRect = to_texture_rect(tile.bgSpriteType)) {
        render_copy(gameState.backgroundTextureHandle, *maybeTextureRect,
                    tileRect);
      }

      if (auto const maybeTextureRect = to_texture_rect(tile.fgSpriteType)) {
        render_copy(gameState.foregroundTextureHandle, *maybeTextureRect,
                    tileRect);
      }
    }
  }
  render_entities();
}

auto static render_entities() -> void {
  auto const& gameState = get_game_state();
  // Player rendering
  auto playerHitboxCpy = gameState.player.entity.hitbox;
  playerHitboxCpy.y -= gameState.get_tile_size() / 4;
  playerHitboxCpy.y -= gameState.camera.pos.y;
  playerHitboxCpy.x -= gameState.camera.pos.x;
  render_copy(gameState.player.entity.textureHandle,
              gameState.player.entity.textureRect, playerHitboxCpy);
}

auto static render_ui_box_normalized(UIBox const& uibox) -> void {
  // draw background
  set_render_draw_color(Color::white);
  render_fill_rect_normalized(uibox.bgRectNormalized);

  // draw foreground
  auto regionCount = uibox.regions.size();

  for (std::size_t i {0}; i < regionCount; i++) {
    auto const currentWord = string_view {uibox.regions[i].text};
    auto const relativeX =
        (static_cast<int>(i) % uibox.columns) * uibox.normalizedXSep;
    auto const relativeY =
        (static_cast<int>(i) / uibox.columns) * uibox.normalizedYSep;

    RectNormalized const printRect = {
        uibox.fgRectNormalized.x + relativeX,
        uibox.fgRectNormalized.y + relativeY,
        uibox.fgRectNormalized.w,
        1 * normalizedSymbolHeight,
    };

    draw_text_line_normalized(currentWord, printRect);
  }

  // draw arrow if menu
  if (uibox.is_menu()) {
    auto const relativeX =
        (uibox.selection % uibox.columns) * uibox.normalizedXSep;
    auto const relativeY =
        (uibox.selection / uibox.columns) * uibox.normalizedYSep;

    auto const selectionRect = RectNormalized {
        (uibox.fgRectNormalized.x + relativeX - normalizedSymbolWidth),
        (uibox.fgRectNormalized.y + relativeY), normalizedSymbolWidth,
        normalizedSymbolHeight};

    auto const& gameState = get_game_state();
    render_copy_normalized(gameState.characterTextureHandle,
                           gameState.symbolSet[translate_char('>')],
                           selectionRect);
  }
}

auto static render_ui_normalized() -> void {
  auto const& gameState = get_game_state();
  if (gameState.state == GameState::State::message) {
    // TODO
  } else if (gameState.state == GameState::State::battle) {
    render_battle_normalized();
  }
  for (auto const& uigroup : uiGroupStack) {
    for (auto const& uibox : uigroup) {
      render_ui_box_normalized(uibox);
    }
  }
}

auto static render_battle_normalized() -> void {
  set_render_draw_color(Color::white);
  render_clear();

  auto const nameOpponent =
      RectNormalized {normalizedSymbolWidth, 0, normalizedSymbolWidth * 10,
                      normalizedSymbolHeight};

  auto const nameSelf = RectNormalized {
      1.0 - normalizedSymbolWidth * 10, 2. / 3. - 2 * normalizedSymbolHeight,
      normalizedSymbolWidth * 10, normalizedSymbolHeight};

  char hp1String[4];
  char hp2String[4];

  sprintf(hp1String, "%d", currentOpponent->hp);
  sprintf(hp2String, "%d", currentPokemon->hp);

  draw_text_line_normalized(currentOpponent->name, nameOpponent);
  draw_text_line_normalized(currentPokemon->name, nameSelf);

  auto hpOpponentRect = RectNormalized {
      nameOpponent.x, nameOpponent.y + normalizedSymbolHeight,
      nameOpponent.w * currentOpponent->hp / currentOpponent->maxHp,
      nameOpponent.h};
  auto hpSelfRect = RectNormalized {
      nameSelf.x, nameSelf.y + normalizedSymbolHeight,
      nameSelf.w * currentPokemon->hp / currentPokemon->maxHp, nameSelf.h};

  set_render_draw_color(Color::green);
  render_fill_rect_normalized(hpOpponentRect);
  render_fill_rect_normalized(hpSelfRect);

  hpSelfRect.w = nameSelf.w;
  hpOpponentRect.w = nameOpponent.w;
  set_render_draw_color(Color::black);
  render_draw_rect_normalized(hpOpponentRect);
  render_draw_rect_normalized(hpSelfRect);

  // TODO: Add numbers to the character set
  /* draw_text_line(hp1String, &hp1); */
  /* draw_text_line(hp2String, &hp2); */
}

auto static draw_text_line(string_view str, Rect const lineRect)
    -> string_view {
  // check length of rect
  // check length of word in string
  // before writing every word see if word fits into remaining space
  // return location in string where left off

  auto const& gameState = get_game_state();
  auto const symbolSize = gameState.get_symbol_size();

  int currentWordX = lineRect.x;
  auto remainingCharSpace = static_cast<std::size_t>(lineRect.w / symbolSize);
  while (not str.empty()) {
    auto const firstSpaceIndex = str.find_first_of(' ');
    auto const wordLength =
        firstSpaceIndex == str.npos ? str.size() : firstSpaceIndex;

    if (wordLength <= remainingCharSpace) {
      // do the stuff
      auto const wordRect =
          Rect {currentWordX, lineRect.y,
                static_cast<int>(wordLength) * symbolSize, symbolSize};

      string_view const word {
          str.data(), static_cast<decltype(str)::size_type>(wordLength)};
      draw_word(word, wordRect);
      remainingCharSpace -= wordLength + 1;
    } else {
      break;
    }

    str.remove_prefix(wordLength);
    auto nextWordStartIndex = str.find_first_not_of(' ');
    if (nextWordStartIndex == str.npos) {
      str.remove_prefix(str.size());
    } else {
      str.remove_prefix(nextWordStartIndex);
    }
    currentWordX += static_cast<int>(wordLength + 1) * symbolSize;
  }
  return str;
}

auto static draw_text_line_normalized(string_view str,
                                      RectNormalized const lineRect)
    -> string_view {
  auto const& windowSize = get_program().get_window().get_size();
  return draw_text_line(str, to_screen_space(lineRect, windowSize));
}

auto static draw_word(string_view const word, Rect const wordRect) -> void {
  assert(word.find_first_of(' ') == word.npos);

  auto const& gameState = get_game_state();
  auto const symbolSize = gameState.get_symbol_size();

  auto symbolRect = wordRect;
  symbolRect.w = symbolSize;
  symbolRect.h = symbolSize;

  for (auto const c : word) {
    draw_character(c, symbolRect);
    symbolRect.x += symbolSize;
  }
}

auto static draw_character(char const ch, Rect const chRect) -> void {
  auto const& gameState = get_game_state();
  auto const i = translate_char(ch);
  render_copy(gameState.characterTextureHandle, gameState.symbolSet[i], chRect);
}

auto static render_editing_level() -> void {
  set_render_draw_color(Color::black);
  render_clear();

  auto const& gameState = get_game_state();

  auto const currentTileSize =
      gameState.get_tile_size() + gameState.editingZoom;

  for (auto const& [pos, tile] : gameState.editMap) {
    auto const tileRect =
        get_tile_rect(pos, currentTileSize, gameState.editingCamera);

    if (auto const maybeTextureRect = to_texture_rect(tile.bgSpriteType)) {
      render_copy(gameState.backgroundTextureHandle, *maybeTextureRect,
                  tileRect);
    }

    if (auto const maybeTextureRect = to_texture_rect(tile.fgSpriteType)) {
      render_copy(gameState.foregroundTextureHandle, *maybeTextureRect,
                  tileRect);
    }
  }
}

auto static render_editing_ui() -> void {
  auto const& gameState = get_game_state();
  auto const currentTileSize =
      gameState.get_tile_size() + gameState.editingZoom;
  auto const selection =
      get_tile_rect({gameState.editingSelectionX, gameState.editingSelectionY},
                    currentTileSize, gameState.editingCamera);

  set_render_draw_color(Color::red);
  render_draw_rect(selection);

  auto const bgEditTileSelection =
      Rect {0, 0, gameState.get_tile_size(), gameState.get_tile_size()};
  if (auto const maybeTextureRect =
          to_texture_rect(gameState.bgEditItemSprite)) {
    render_copy(gameState.backgroundTextureHandle, *maybeTextureRect,
                bgEditTileSelection);
  }
  set_render_draw_color(Color::blue);
  render_draw_rect(bgEditTileSelection);

  auto const fgEditTileSelection =
      Rect {gameState.get_tile_size(), 0, gameState.get_tile_size(),
            gameState.get_tile_size()};

  if (auto const maybeTextureRect =
          to_texture_rect(gameState.fgEditItemSprite)) {
    render_copy(gameState.foregroundTextureHandle, *maybeTextureRect,
                fgEditTileSelection);
  }
  set_render_draw_color(Color::magenta);
  render_draw_rect(fgEditTileSelection);
}
