#include "sdlmain.hh"

#include "core.hh"
#include "graphics.hh"
#include "input.hh"
#include "sdlrender.hh"

#include <SDL.h>

#include <iostream>
#include <optional>
#include <sstream>
#include <vector>

using std::string_view, std::vector;

SDL_Renderer* gSDLRenderer;
PlatformWindow* gWindow;

[[noreturn]] auto throw_sdl_error() -> void {
  auto const* err = SDL_GetError();
  std::cerr << err << '\n';
  throw std::runtime_error {err};
}

PlatformWindow::PlatformWindow(int width, int height, int scale)
    : m_width {width * scale}, m_height {height * scale}, m_scale {scale} {
  assert(width > 0);
  assert(height > 0);
  assert(scale > 0);
  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    throw_sdl_error();
  }
  m_window = SDL_CreateWindow("Pokemon test", SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED, m_width, m_height,
                              SDL_WINDOW_SHOWN);
  if (m_window == nullptr) {
    throw_sdl_error();
  }
  m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED);
  if (m_renderer == nullptr) {
    throw_sdl_error();
  }
  if (gWindow != nullptr) {
    auto const* errMsg = "Only one Window can exist at once.";
    std::cerr << errMsg << '\n';
    throw std::runtime_error {errMsg};
  }

  auto* exePathString = SDL_GetBasePath();
  if (exePathString == nullptr) {
    throw;
  }

  m_exePath = std::filesystem::path {exePathString}.lexically_normal();
  m_spritesPath = (m_exePath / "../sprites").lexically_normal();
  m_mapsPath = (m_exePath / "../maps").lexically_normal();

  SDL_free(exePathString);

  gWindow = this;
}

PlatformWindow::~PlatformWindow() noexcept {
  SDL_DestroyRenderer(m_renderer);
  SDL_DestroyWindow(m_window);
  SDL_Quit();
}

auto get_mod_state() -> ModBitSet { return SDL_GetModState(); }

auto static to_keycode(SDL_Keycode const key) -> Keycode {
  switch (key) {
  case SDLK_RETURN:
    return Keycode::enter;
  case SDLK_BACKSPACE:
    return Keycode::backspace;
  case SDLK_TAB:
    return Keycode::tab;
  case SDLK_ESCAPE:
    return Keycode::escape;
  case SDLK_SPACE:
    return Keycode::space;
  case SDLK_EXCLAIM:
    return Keycode::exclamation;
  case SDLK_QUOTEDBL:
    return Keycode::quote;
  case SDLK_HASH:
    return Keycode::hash;
  case SDLK_DOLLAR:
    return Keycode::dollar;
  case SDLK_PERCENT:
    return Keycode::percent;
  case SDLK_AMPERSAND:
    return Keycode::ampersand;
  case SDLK_QUOTE:
    return Keycode::apostrophe;
  case SDLK_LEFTPAREN:
    return Keycode::openparen;
  case SDLK_RIGHTPAREN:
    return Keycode::closeparen;
  case SDLK_ASTERISK:
    return Keycode::asterisk;
  case SDLK_PLUS:
    return Keycode::plus;
  case SDLK_COMMA:
    return Keycode::comma;
  case SDLK_MINUS:
    return Keycode::minus;
  case SDLK_PERIOD:
    return Keycode::period;
  case SDLK_SLASH:
    return Keycode::slash;
  case SDLK_COLON:
    return Keycode::colon;
  case SDLK_SEMICOLON:
    return Keycode::semicolon;
  case SDLK_LESS:
    return Keycode::less;
  case SDLK_GREATER:
    return Keycode::greater;
  case SDLK_QUESTION:
    return Keycode::question;
  case SDLK_AT:
    return Keycode::at;
  case SDLK_LEFTBRACKET:
    return Keycode::openbracket;
  case SDLK_BACKSLASH:
    return Keycode::backslash;
  case SDLK_RIGHTBRACKET:
    return Keycode::closebracket;
  case SDLK_CARET:
    return Keycode::caret;
  case SDLK_UNDERSCORE:
    return Keycode::underscore;
  case SDLK_BACKQUOTE:
    return Keycode::grave;
  case SDLK_UP:
    return Keycode::up;
  case SDLK_DOWN:
    return Keycode::down;
  case SDLK_LEFT:
    return Keycode::left;
  case SDLK_RIGHT:
    return Keycode::right;
  case SDLK_0:
  case SDLK_1:
  case SDLK_2:
  case SDLK_3:
  case SDLK_4:
  case SDLK_5:
  case SDLK_6:
  case SDLK_7:
  case SDLK_8:
  case SDLK_9:
    return static_cast<Keycode>(
        static_cast<int>(Keycode::key0) +
        (static_cast<int>(key) - static_cast<int>(SDLK_0)));
  case SDLK_a:
  case SDLK_b:
  case SDLK_c:
  case SDLK_d:
  case SDLK_e:
  case SDLK_f:
  case SDLK_g:
  case SDLK_h:
  case SDLK_i:
  case SDLK_j:
  case SDLK_k:
  case SDLK_l:
  case SDLK_m:
  case SDLK_n:
  case SDLK_o:
  case SDLK_p:
  case SDLK_q:
  case SDLK_r:
  case SDLK_s:
  case SDLK_t:
  case SDLK_u:
  case SDLK_v:
  case SDLK_w:
  case SDLK_x:
  case SDLK_y:
  case SDLK_z:
    return static_cast<Keycode>(
        static_cast<int>(Keycode::a) +
        (static_cast<int>(key) - static_cast<int>(SDLK_a)));
  default: {
    auto ss = std::stringstream {};
    ss << "SDL_Keycode -> Keycode conversion not defined for key #"
       << static_cast<int>(key) << "('" << static_cast<char>(key) << "')\n";
    std::cerr << ss.str();

    return Keycode::invalid;
  }
  }
}

auto static to_event(SDL_Event const& e) -> Event {
  switch (e.type) {
  case SDL_QUIT:
    return Event {Event::Quit {}};
  case SDL_KEYDOWN: {
    return Event {Event::Key {to_keycode(e.key.keysym.sym)}};
  }
  case SDL_MOUSEMOTION:
    return Event {
        Event::Mouse::Motion {(e.motion.state & SDL_BUTTON_RMASK) != 0U,
                              {e.motion.xrel, e.motion.yrel}}};
  case SDL_MOUSEWHEEL:
    return Event {Event::Mouse::Wheel {e.wheel.y}};
  case SDL_MOUSEBUTTONDOWN:
    return Event {Event::Mouse::Button {{e.button.x, e.button.y},
                                        e.button.button == SDL_BUTTON_LEFT}};
  default: {
    auto ss = std::stringstream {};
    ss << "SDL_Event -> Event conversion not defined for event #"
       << static_cast<int>(e.type) << ".\n";
    std::cerr << ss.str();

    return Event {};
  }
  }
}

auto poll_event() -> std::optional<Event> {
  SDL_Event event;
  if (SDL_PollEvent(&event) != 0) {
    return to_event(event);
  }

  return std::nullopt;
}

Window::Window(PlatformWindow& platformWindow)
    : m_platformWindow {platformWindow},
      m_exePath {platformWindow.get_exe_path()},
      m_spritesPath {platformWindow.get_sprites_path()},
      m_mapsPath {platformWindow.get_maps_path()},
      m_scale {platformWindow.get_scale()},
      m_width {platformWindow.get_width()}, m_height {
                                                platformWindow.get_height()} {}

ResourceManager::ResourceManager(
    PlatformResourceManager& platformResourceManager)
    : m_platformResourceManager {platformResourceManager} {}

auto ResourceManager::create_texture(std::string_view const fileName)
    -> std::size_t {
  return m_platformResourceManager.create_texture(fileName);
}

auto main(int argc, char** argv) -> int {
  PlatformWindow platformWindow {Window::baseWidth, Window::baseHeight};
  PlatformResourceManager resMngr;

  auto window = Window {platformWindow};
  auto resourceManager = ResourceManager {resMngr};

  auto program = Program {window, resourceManager};

  run(program);

  return 0;
}
