#include "level.hh"

#include "core.hh"
#include "gamestate.hh"
#include "graphics.hh"
#include "ui.hh"

#include <cassert>
#include <filesystem>
#include <fstream>
#include <string>
#include <string_view>

using std::string_view, std::to_string;

namespace fs = std::filesystem;

Map::Map(string_view const fileName)
    : Map {get_program().get_maps_path() / fileName} {}

Map::Map(fs::path const& filePath) {
  // filePath must point to an actual file.
  if (not fs::is_regular_file(filePath)) {
    throw;
  }

  auto ifs = std::ifstream {filePath};
  auto word = std::string {};
  ifs >> word;
  if (word != "JMAPFORMAT") {
    throw;
  }
  ifs >> m_width;
  ifs >> m_height;

  for (auto i = 0; i < m_width * m_height; ++i) {
    auto tile = Tile {};
    auto tmp = 0;
    ifs >> tmp;
    tile.solid = static_cast<bool>(tmp);
    ifs >> tmp;
    tile.encounterType = static_cast<EncounterType>(tmp);
    ifs >> tmp;
    tile.fgSpriteType = static_cast<Tile::FGSpriteType>(tmp);
    ifs >> tmp;
    tile.bgSpriteType = static_cast<Tile::BGSpriteType>(tmp);
    m_tiles.emplace_back(tile);
  }

  if (ifs.fail()) {
    throw;
  }
}

// Backporting erase_if from c++20.
template <typename Pred>
auto erase_if(std::unordered_map<Point, Tile>& c, Pred const pred)
    -> std::size_t {
  auto const old_size = c.size();
  for (auto i = c.begin(), last = c.end(); i != last;) {
    if (pred(*i)) {
      i = c.erase(i);
    } else {
      ++i;
    }
  }
  return old_size - c.size();
}

Map::Map(std::unordered_map<Point, Tile> editMap) {
  erase_if(editMap, [](auto const& keyval) {
    auto const& tile = keyval.second;
    return tile.bgSpriteType == Tile::BGSpriteType::none and
           tile.fgSpriteType == Tile::FGSpriteType::none;
  });

  auto const [minXIt, maxXIt] =
      std::minmax_element(editMap.cbegin(), editMap.cend(),
                          [](auto const& keyval1, auto const& keyval2) {
                            auto const& pos1 = keyval1.first;
                            auto const& pos2 = keyval2.first;
                            return pos1.x < pos2.x;
                          });
  auto const [minYIt, maxYIt] =
      std::minmax_element(editMap.cbegin(), editMap.cend(),
                          [](auto const& keyval1, auto const& keyval2) {
                            auto const& pos1 = keyval1.first;
                            auto const& pos2 = keyval2.first;
                            return pos1.y < pos2.y;
                          });

  auto const minX = minXIt->first.x;
  auto const maxX = maxXIt->first.x;
  auto const minY = minYIt->first.y;
  auto const maxY = maxYIt->first.y;

  m_width = maxX - minX + 1;
  m_height = maxY - minY + 1;
  assert(m_width >= 0);
  assert(m_height >= 0);

  m_tiles = std::vector<Tile>(m_width * m_height);

  for (auto const& [pos, tile] : editMap) {
    // Turn coordinates to range 0..width/height.
    auto const normalizedX = pos.x - minX;
    auto const normalizedY = pos.y - minY;
    tile_at({normalizedX, normalizedY}) = tile;
  }
}

auto Map::save(string_view const fileName) const -> bool {
  auto const normalizedPath = fs::path {fileName}.lexically_normal();
  // fileName must not end up being a relative path to some other folder, but
  // simply a filename.
  if (normalizedPath.has_parent_path()) {
    throw;
  }

  auto const& mapsDirectory = get_program().get_maps_path();
  auto const filePath = mapsDirectory / normalizedPath;
  return save(filePath);
}

auto Map::save(fs::path const& filePath) const -> bool {
  auto const normalizedPath = filePath.lexically_normal();
  auto const& mapsDirectory = get_program().get_maps_path();
  if (normalizedPath.parent_path() != mapsDirectory) {
    throw;
  }

  if (fs::exists(normalizedPath)) {
    // TODO: prompt user to overwrite file.
    return false;
  }

  // filePath must point to an actual file.
  if (not normalizedPath.has_filename()) {
    throw;
  }

  std::ofstream ofs {normalizedPath};
  ofs << "JMAPFORMAT\n";
  ofs << to_string(m_width) << ' ';
  ofs << to_string(m_height) << "\n";

  auto index = 0;
  for (auto const& tile : m_tiles) {
    ofs << '\n';
    ofs << to_string(static_cast<int>(tile.solid)) << ' ';
    ofs << to_string(static_cast<int>(tile.encounterType)) << ' ';
    ofs << to_string(static_cast<int>(tile.fgSpriteType)) << ' ';
    ofs << to_string(static_cast<int>(tile.bgSpriteType));

    // create some space between each row.
    if (++index == m_width) {
      index = 0;
      ofs << '\n';
    }
  }

  if (ofs.fail()) {
    throw;
  }

  return true;
}

auto load_map(MapID newMap) -> void {
  auto& gameState = get_game_state();
  gameState.currentMapID = newMap;
  uiGroupStack.clear();
}
