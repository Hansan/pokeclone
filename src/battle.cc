#include "battle.hh"

#include "core.hh"
#include "data.hh"
#include "gamestate.hh"
#include "pokemon.hh"
#include "ui.hh"

#include <algorithm>
#include <cassert>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <ctime>

using std::find_if;

auto static exit_battle() -> void;
auto static execute_move(Pokemon& atkPokemon, Pokemon& defPokemon, int i)
    -> void;

Pokemon* currentPokemon;
Pokemon* currentOpponent;

auto use_move() -> void {
  assert(not uiGroupStack.empty());
  auto* menuBox = uiGroupStack.back().get_menu_box();
  assert(menuBox);
  execute_move(*currentPokemon, *currentOpponent, menuBox->selection);
}

auto poke_battle() -> void {
  auto& gameState = get_game_state();
  gameState.state = GameState::State::battle;
  set_menu(UIBox::MenuID::battle);

  create_party(); // temporary
  auto& party = gameState.player.party;
  currentPokemon =
      &(*find_if(party.begin(), party.end(),
                 [](auto const& pokemon) { return pokemon.hp > 0; }));
}

auto run_from_battle() -> void {
  auto const rng = get_program().get_random_device()();

  if ((rng % 3) != 0) {
    exit_battle();
  } else {
    puts("CAN'T ESCAPE");
    fflush(stdout);
  }
}

auto set_moves() -> void {
  assert(not uiGroupStack.empty());
  auto* menuBox = uiGroupStack.back().get_menu_box();
  assert(menuBox);
  menuBox->regions[0].text = currentPokemon->moves[0].name;
  menuBox->regions[1].text = currentPokemon->moves[1].name;
  menuBox->regions[2].text = currentPokemon->moves[2].name;
  menuBox->regions[3].text = currentPokemon->moves[3].name;
}

auto static execute_move(Pokemon& atkPokemon, Pokemon& defPokemon, int i)
    -> void {
  /* sprintf(mes, "%s used %s!\n", atkPokemon.name, atkPokemon.moves[i].name);
   */
  int hploss = atkPokemon.moves[i].power - defPokemon.stats.def;
  if (hploss < 0) {
    hploss = 0;
  }
  defPokemon.hp -= hploss;
  if (defPokemon.hp <= 0) {
    exit_battle();
  }
}

auto static exit_battle() -> void {
  auto& gameState = get_game_state();
  gameState.state = GameState::State::overworld;
  set_menu(UIBox::MenuID::none);
}
