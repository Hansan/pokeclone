#include "core.hh"

#include "data.hh"
#include "gamestate.hh"
#include "graphics.hh"
#include "input.hh"
#include "ui.hh"

bool quitFlag;

Program* gProgram {nullptr};
GameState* gGameState {nullptr};

auto get_program() -> Program& { return *gProgram; }
auto get_game_state() -> GameState& { return *gGameState; }

auto run(Program& program) -> void {
  gProgram = &program;
  auto gameState = GameState {program};
  gGameState = &gameState;

  // main loop
  while (not quitFlag) {
    handle_events();
    render();
  }
}
