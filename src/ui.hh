#pragma once

#include "data.hh"
#include "gamestate.hh"
#include "move.hh"
#include "utils.hh"

#include <algorithm>
#include <cassert>
#include <vector>

struct UIText {
  const char* text;
  void (*execute)() = nullptr;
};

struct UIBox {
  enum class MenuID {
    none,
    mainMenu,
    pause,
    pokedex,
    pokemon,
    items,
    trainer,
    save,
    options,
    battle,
    battlemoves,
    previous,
  };

  int columns = 1;
  MenuID menuid = MenuID::none;
  std::vector<UIText> regions = {};
  int selection = 0;
  RectNormalized bgRectNormalized = {};
  RectNormalized fgRectNormalized = {};
  double normalizedXSep = GameState::normalizedTileSize.w;
  double normalizedYSep = GameState::normalizedTileSize.h;

  auto static message_box(UIText msg) -> UIBox;

  [[nodiscard]] auto is_menu() const { return menuid != MenuID::none; }
  [[nodiscard]] auto
  calculate_selection_after_move(Direction direction) const noexcept -> int;
  [[nodiscard]] auto move_selection(Direction direction) -> void;
};

class UIGroup {
  std::vector<UIBox> uiBoxes;

public:
  void add(UIBox const& uiBox) {
    // there should never be two menus added to a group
    if (uiBox.is_menu()) {
      assert(not has_menu_box());
    }
    uiBoxes.push_back(uiBox);
  }

  auto get_menu_box() -> UIBox* {
    auto menuIt = std::find_if(uiBoxes.begin(), uiBoxes.end(),
                               [](auto& uiBox) { return uiBox.is_menu(); });
    if (menuIt == uiBoxes.end()) {
      return nullptr;
    }
    return &(*menuIt);
  }

  [[nodiscard]] auto has_menu_box() const -> bool {
    return std::any_of(uiBoxes.begin(), uiBoxes.end(),
                       [](auto const& uiBox) { return uiBox.is_menu(); });
  }

  [[nodiscard]] auto empty() const { return uiBoxes.empty(); }
  auto begin() { return uiBoxes.begin(); }
  auto end() { return uiBoxes.end(); }
  [[nodiscard]] auto begin() const { return uiBoxes.begin(); }
  [[nodiscard]] auto end() const { return uiBoxes.end(); }
  [[nodiscard]] auto cbegin() const { return begin(); }
  [[nodiscard]] auto cend() const { return begin(); }
};

using UIGroupStack = std::vector<UIGroup>;

auto set_menu(UIBox::MenuID menuID) -> void;

auto ui_interact() -> void;
auto ui_cancel() -> void;

auto open_pause_menu() -> void;
auto exit_menu() -> void;

auto unlock_pause_menu() -> void;

extern UIGroupStack uiGroupStack;
