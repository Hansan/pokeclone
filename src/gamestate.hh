#pragma once

#include "data.hh"
#include "level.hh"
#include "pokemon.hh"
#include "program.hh"
#include "utils.hh"

#include <cassert>
#include <ctime>
#include <string_view>
#include <unordered_map>

struct GameState {
  enum class State {
    nothing,
    overworld,
    message,
    battle,
    editing,
  };

  struct Entity {
    enum class ID {
      player,
      wall,
    };

    ID id {};
    Point pos {};
    Rect hitbox {};
    std::size_t textureHandle {};
    Rect textureRect {};
  };

  struct Player {
    Entity entity;
    std::array<Pokemon, 6> party;
  };

  struct Camera {
    Point pos {};
  };

  explicit GameState(Program& program);

  Player player {};

  Camera camera {};
  Camera editingCamera {};

  MapID currentMapID {MapID::none};

  std::unordered_map<MapID, Map> maps {};

  std::unordered_map<Point, Tile> editMap {};

  // player sprite locations in sprite sheet
  Rect sprite[4] {};
  // character location in sprite sheet
  Rect symbolSet[100] {};

  // texture created from sprite sheet
  std::size_t characterTextureHandle {};

  std::size_t backgroundTextureHandle {};
  std::size_t foregroundTextureHandle {};

  State state {};
  State stateBeforeEdit {};
  int editingZoom {};
  int editingSelectionX {};
  int editingSelectionY {};
  Tile::FGSpriteType fgEditItemSprite {};
  Tile::BGSpriteType bgEditItemSprite {};

  Program& m_program;

  [[nodiscard]] auto get_tile_size() const noexcept -> int {
    return m_program.get_window().get_scale() * baseTileSize;
  }

  [[nodiscard]] auto get_symbol_size() const noexcept -> int {
    return m_program.get_window().get_scale() * baseSymbolSize;
  }

  auto static constexpr normalizedTileSize = RectNormalized::Size {
      static_cast<double>(baseTileSize) / Window::baseWidth,
      static_cast<double>(baseTileSize) / Window::baseHeight};

  auto static constexpr normalizedSymbolSize = RectNormalized::Size {
      static_cast<double>(baseSymbolSize) / Window::baseWidth,
      static_cast<double>(baseSymbolSize) / Window::baseHeight};
};
