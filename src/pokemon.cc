#include "pokemon.hh"

#include "battle.hh"
#include "core.hh"
#include "data.hh"

#include <array>

using std::array;

Move static constexpr scratch = {
    "Scratch", 10, 100, 20, PokeType::normal,
};

Move static constexpr noMove = {
    "-", 0, 0, 0, PokeType::none,
};

Pokemon charmander = {
    4,
    "CHARMANDER",
    PokeStatus::none,
    PokeType::fire,
    PokeType::none,
    {},
    PokemonStats {},
    0,
    0,
    0,
    0,
};

/*
enum move_enum {
        NOMOVE,
        SCRATCH,
};

Move moves[] = {
        noMove,
        scratch,
};
*/

auto create_party() -> void {
  auto& party = get_game_state().player.party;
  Pokemon& currPokemon = party[0];
  currPokemon = charmander;
  currPokemon.lvl = 5;
  currPokemon.moves[0] = scratch;
  currPokemon.moves[1] = noMove;
  currPokemon.moves[2] = noMove;
  currPokemon.moves[3] = noMove;
  currPokemon.stats.atk = 12;
  currPokemon.stats.def = 11;
  currPokemon.stats.spd = 12;
  currPokemon.stats.spc = 12;
  currPokemon.maxHp = 20;
  currPokemon.hp = 20;

  // temporary
  currentOpponent = &charmander;
  currentOpponent->lvl = 5;
  currentOpponent->moves[0] = scratch;
  currentOpponent->moves[1] = noMove;
  currentOpponent->moves[2] = noMove;
  currentOpponent->moves[3] = noMove;
  currentOpponent->stats.atk = 12;
  currentOpponent->stats.def = 11;
  currentOpponent->stats.spd = 12;
  currentOpponent->stats.spc = 12;
  currentOpponent->maxHp = 20;
  currentOpponent->hp = 20;
}
