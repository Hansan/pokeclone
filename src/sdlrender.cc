#include "sdlrender.hh"

#include "core.hh"
#include "sdlmain.hh"

#include <SDL.h>

using std::string_view;

auto PlatformResourceManager::create_surface(string_view const bmpFileName)
    -> std::size_t {
  m_surfaces.emplace_back(bmpFileName);
  return m_surfaces.size() - 1;
}

auto PlatformResourceManager::create_texture(Surface const& surface)
    -> std::size_t {
  m_textures.emplace_back(surface);
  return m_textures.size() - 1;
}

auto PlatformResourceManager::create_texture(string_view const bmpFileName)
    -> std::size_t {
  return create_texture(Surface {bmpFileName});
}

Surface::Surface(string_view const bmpFileName) {
  auto const filePath = get_program().get_sprites_path() / bmpFileName;

  // SDL_LoadBMP expects a char* while filePath.c_str() returns
  // path::value_type*. In order to ensure a char* is used, we call
  // filePath.string() before calling c_str().
  m_surface = SDL_LoadBMP(filePath.string().c_str());
  if (m_surface == nullptr) {
    throw_sdl_error();
  }
}

Surface::Surface(Surface&& other) noexcept { *this = std::move(other); }

auto Surface::operator=(Surface&& other) noexcept -> Surface& {
  if (this == &other) {
    return *this;
  }
  SDL_FreeSurface(m_surface);
  m_surface = std::exchange(other.m_surface, nullptr);
  return *this;
}

Texture::Texture(Surface const& surface) {
  SDL_SetColorKey(surface.get(), SDL_TRUE,
                  SDL_MapRGB(surface.get()->format, 0xFF, 0, 0xFF));
  auto* renderer =
      get_program().get_window().get_platform_window().get_renderer();
  m_texture = SDL_CreateTextureFromSurface(renderer, surface.get());
  if (m_texture == nullptr) {
    throw_sdl_error();
  }
}

Texture::Texture(string_view const bmpFileName)
    : Texture {Surface {bmpFileName}} {}

Texture::Texture(Texture&& other) noexcept { *this = std::move(other); }

auto Texture::operator=(Texture&& other) noexcept -> Texture& {
  if (this == &other) {
    return *this;
  }
  if (m_texture != nullptr) {
    SDL_DestroyTexture(m_texture);
  }
  m_texture = std::exchange(other.m_texture, nullptr);
  return *this;
}

Texture::~Texture() noexcept {
  if (m_texture != nullptr) {
    SDL_DestroyTexture(m_texture);
  }
}

auto static constexpr to_SDL_Rect(Rect const r) -> SDL_Rect {
  return {r.x, r.y, r.w, r.h};
}

auto render_copy(std::size_t textureHandle, Rect const srcRect,
                 Rect const dstRect) -> void {
  auto const srcSdlRect = to_SDL_Rect(srcRect);
  auto const dstSdlRect = to_SDL_Rect(dstRect);
  auto& texture = get_program()
                      .get_resource_manager()
                      .get_platform_resource_manager()
                      .get_texture(textureHandle);
  auto* renderer =
      get_program().get_window().get_platform_window().get_renderer();
  SDL_RenderCopy(renderer, texture.get(), &srcSdlRect, &dstSdlRect);
}

auto render_copy_normalized(std::size_t textureHandle, Rect const srcRect,
                            RectNormalized const dstRect) -> void {
  auto const& window = get_program().get_window();
  auto const screenSpaceRect = to_screen_space(dstRect, window.get_size());
  render_copy(textureHandle, srcRect, screenSpaceRect);
}

auto render_clear() -> void {
  auto* renderer =
      get_program().get_window().get_platform_window().get_renderer();
  SDL_RenderClear(renderer);
}

auto render_present() -> void {
  auto* renderer =
      get_program().get_window().get_platform_window().get_renderer();
  SDL_RenderPresent(renderer);
}

auto render_draw_rect(Rect const rect) -> void {
  auto* renderer =
      get_program().get_window().get_platform_window().get_renderer();
  auto const sdlRect = to_SDL_Rect(rect);
  SDL_RenderDrawRect(renderer, &sdlRect);
}

auto render_draw_rect_normalized(RectNormalized const rect) -> void {
  auto const& window = get_program().get_window();
  render_draw_rect(to_screen_space(rect, window.get_size()));
}

auto render_fill_rect(Rect const rect) -> void {
  auto* renderer =
      get_program().get_window().get_platform_window().get_renderer();
  auto const sdlRect = to_SDL_Rect(rect);
  SDL_RenderFillRect(renderer, &sdlRect);
}

auto render_fill_rect_normalized(RectNormalized const rect) -> void {
  auto const& window = get_program().get_window();
  render_fill_rect(to_screen_space(rect, window.get_size()));
}

auto set_render_draw_color(Color::RGBA const c) -> void {
  auto* renderer =
      get_program().get_window().get_platform_window().get_renderer();
  SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, c.a);
}
