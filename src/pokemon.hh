#pragma once

#include <array>

enum class PokeStatus {
  none,
  burn,
  sleep,
  poison,
  frozen,
  paralyzed,
  fainted,
};

enum class PokeType {
  none,
  normal,
  fighting,
  flying,
  poison,
  ground,
  rock,
  bug,
  ghost,
  fire,
  water,
  grass,
  electric,
  psychic,
  ice,
  dragon,
};

struct Move {
  const char* name {};
  int pp {};
  int accuracy {};
  int power {};
  PokeType type {};
};

struct PokemonStats {
  int atk {};
  int def {};
  int spd {};
  int spc {};
};

struct Pokemon {
  int id {};
  const char* name {};
  PokeStatus status {};
  PokeType type1 {};
  PokeType type2 {};
  std::array<Move, 4> moves {};
  PokemonStats stats {};
  int exp {};
  int lvl {};
  int maxHp {};
  int hp {};
};

auto create_party() -> void;
