#pragma once

#include "utils.hh"
#include "window.hh"

#include <array>

auto static constexpr baseTileSize = 16;
auto static constexpr baseSymbolSize = 8;

auto constexpr textureTileSize = 16;
auto constexpr textureTileWidthCount = 16;
auto constexpr textureTileHeightCount = 16;
auto constexpr textureTileCount =
    textureTileWidthCount * textureTileHeightCount;

auto constexpr texture_rect_array() {
  std::array<Rect, textureTileCount> newTextureRects {};
  auto constexpr textureTilesRect =
      Rect {0, 0, textureTileWidthCount, textureTileHeightCount};
  for (auto y = 0; y < textureTileHeightCount; ++y) {
    for (auto x = 0; x < textureTileWidthCount; ++x) {
      auto const i = textureTilesRect.to_index({x, y});
      newTextureRects[i] = {(x * textureTileSize), (y * textureTileSize),
                            textureTileSize, textureTileSize};
    }
  }
  return newTextureRects;
}

auto constexpr textureRects = texture_rect_array();

enum class Controls {
  up,
  down,
  left,
  right,
  a,
  b,
  start,
  select,
};
