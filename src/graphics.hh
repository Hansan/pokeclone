#pragma once

#include "utils.hh"

auto render() -> void;
auto get_rgba_from_pixel(int pixelPos) -> Color::RGBA;
