#pragma once

#include "sdlmain.hh"

#include <string_view>

class PlatformResourceManager;

class ResourceManager {
public:
  explicit ResourceManager(PlatformResourceManager& platformResourceManager);
  ResourceManager(ResourceManager const&) = delete;
  ResourceManager(ResourceManager&&) noexcept = delete;
  auto operator=(ResourceManager const&) = delete;
  auto operator=(ResourceManager&&) noexcept = delete;
  ~ResourceManager() noexcept = default;

  auto create_texture(std::string_view fileName) -> std::size_t;

  auto get_platform_resource_manager() noexcept -> PlatformResourceManager& {
    return m_platformResourceManager;
  }

private:
  PlatformResourceManager& m_platformResourceManager;
};
