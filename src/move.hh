#pragma once

enum class Direction { up, down, left, right };

auto move_player(Direction direction) -> void;
