#pragma once

#include "input.hh"

#include <SDL.h>

#include <filesystem>
#include <optional>

class PlatformWindow {
public:
  explicit PlatformWindow(int width, int height, int scale = 4);
  PlatformWindow(PlatformWindow const&) = delete;
  auto operator=(PlatformWindow const&) = delete;
  PlatformWindow(PlatformWindow&&) = delete;
  auto operator=(PlatformWindow&&) = delete;
  ~PlatformWindow() noexcept;

  auto get_renderer() -> SDL_Renderer* { return m_renderer; }

  [[nodiscard]] auto get_exe_path() const -> std::filesystem::path const& {
    return m_exePath;
  }

  [[nodiscard]] auto get_sprites_path() const -> std::filesystem::path const& {
    return m_spritesPath;
  }

  [[nodiscard]] auto get_maps_path() const -> std::filesystem::path const& {
    return m_mapsPath;
  }

  [[nodiscard]] auto get_scale() const noexcept -> int { return m_scale; }
  [[nodiscard]] auto get_width() const noexcept -> int { return m_width; }
  [[nodiscard]] auto get_height() const noexcept -> int { return m_height; }

private:
  SDL_Window* m_window {nullptr};
  SDL_Renderer* m_renderer {nullptr};

  std::filesystem::path m_exePath {};
  std::filesystem::path m_spritesPath {};
  std::filesystem::path m_mapsPath {};

  int m_width {};
  int m_height {};
  int m_scale {};
};

auto get_mod_state() -> ModBitSet;
auto poll_event() -> std::optional<Event>;
[[noreturn]] auto throw_sdl_error() -> void;
