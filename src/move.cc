#include "move.hh"

#include "battle.hh"
#include "core.hh"
#include "gamestate.hh"
#include "graphics.hh"
#include "level.hh"
#include "ui.hh"
#include "utils.hh"

#include <algorithm>
#include <cassert>
#include <functional>

struct Location {
  MapID id;
  Point pos;

  auto constexpr operator==(Location const& other) const -> bool {
    return id == other.id and pos == other.pos;
  }
};

namespace std {

template <>
struct hash<Location> {
  auto operator()(Location const& loc) const noexcept -> std::size_t {
    auto const h1 = std::hash<decltype(loc.id)> {}(loc.id);
    auto const h2 = std::hash<decltype(loc.pos)> {}(loc.pos);
    return h1 ^ (h2 << 1U);
  }
};

} // namespace std

auto move_player(Direction direction) -> void {
  auto& gameState = get_game_state();
  gameState.player.entity.textureRect =
      gameState.sprite[static_cast<int>(direction)];

  auto const oldPos = gameState.player.entity.pos;

  switch (direction) {
  case Direction::up:
    gameState.player.entity.pos.y += -1;
    break;
  case Direction::down:
    gameState.player.entity.pos.y += 1;
    break;
  case Direction::left:
    gameState.player.entity.pos.x += -1;
    break;
  case Direction::right:
    gameState.player.entity.pos.x += 1;
    break;
  }

  auto const& map = gameState.maps.at(gameState.currentMapID);
  auto const mapRect = map.get_rect();

  if (not mapRect.contains(gameState.player.entity.pos)) {
    gameState.player.entity.pos = oldPos;
    return;
  }

  auto const teleports = std::unordered_map<Location, Location> {
      {{MapID::ashhouse, {7, 1}}, {MapID::pallet, {7, 1}}},
      {{MapID::pallet, {7, 1}}, {MapID::ashhouse, {7, 1}}},
  };

  if (auto const teleportSpot = teleports.find(
          {gameState.currentMapID,
           {gameState.player.entity.pos.x, gameState.player.entity.pos.y}});
      teleportSpot != teleports.end()) {
    load_map(teleportSpot->second.id);
    gameState.player.entity.pos = teleportSpot->second.pos;
  }

  auto const& tile = map.tile_at(gameState.player.entity.pos);

  if (tile.bgSpriteType == Tile::BGSpriteType::doormat) {
    poke_battle();
  }

  gameState.player.entity.hitbox.x =
      gameState.player.entity.pos.x * gameState.get_tile_size();
  gameState.player.entity.hitbox.y =
      gameState.player.entity.pos.y * gameState.get_tile_size();
  gameState.camera.pos.x =
      gameState.player.entity.hitbox.x - 4 * gameState.get_tile_size();
  gameState.camera.pos.y =
      gameState.player.entity.hitbox.y - 4 * gameState.get_tile_size();
}
