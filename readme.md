This is a Pokemon clone. (Please don't sue me)

Controls
--------

* Arrow keys: Move character and cursor
* Z: Accept
* X: Cancel
* Enter: Open/Close menu
* Q: Quit

Dependencies
------------

* SDL2
